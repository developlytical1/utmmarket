package com.example.utmmart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.provider.FontsContractCompat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class addNewProductActivity extends AppCompatActivity {

    private String Pname, Pprice, Pquan, Pcond, Pcategory, Pdesc, saveCurrentDate, saveCurrentTime;
    private Button PAddNewPrdbtn;
    private ImageButton Pbackbtn;
    private ImageView addProdImage;

    private EditText addPname, addPrice, addQuan, addPcond, addPcat, addPdesc;
    private static final int GalleryPick = 1;
    private Uri ImageUri;
    private String productRandomKey, downloadImageUrl;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;


    private FirebaseDatabase mDatabase;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_product);


        Pbackbtn = (ImageButton) findViewById(R.id.PbackBtn);
        PAddNewPrdbtn = (Button) findViewById(R.id.AddProd_btn);
        addProdImage = (ImageView) findViewById(R.id.addNewProductImg);
        addPname = (EditText) findViewById(R.id.addProd_name);
        addPrice = (EditText) findViewById(R.id.addProd_price);
        addQuan = (EditText) findViewById(R.id.addProd_Quantity);
        addPcond = (EditText) findViewById(R.id.addProd_cond);
        addPcat = (EditText) findViewById(R.id.addProd_category);
        addPdesc = (EditText) findViewById(R.id.addProd_desc);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCanceledOnTouchOutside(false);

        mDatabase = FirebaseDatabase.getInstance();
        databaseReference = mDatabase.getReference();
        storageReference = FirebaseStorage.getInstance().getReference();

        firebaseAuth = FirebaseAuth.getInstance();
        addProdImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenGallery();
            }
        });

        Pbackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        PAddNewPrdbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputData();
            }
        });

        addPcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryDialog();
            }
        });

    }

    public void inputData() {
        Pname = addPname.getText().toString().trim();
        Pprice = addPrice.getText().toString().trim();
        Pquan = addQuan.getText().toString().trim();
        Pdesc = addPdesc.getText().toString().trim();
        Pcategory = addPcat.getText().toString().trim();
        Pcond = addPcond.getText().toString().trim();

        if (TextUtils.isEmpty(Pname)) {
            Toast.makeText(this, "Product name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(Pprice)) {
            Toast.makeText(this, "Product price cannot be empty", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pquan)) {
            Toast.makeText(this, "Product quantity cannot be empty", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pdesc)) {
            Toast.makeText(this, "Product description cannot br empty", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pcategory)) {
            Toast.makeText(this, "Please choose your product category", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pcond)) {
            Toast.makeText(this, "Product condition cannot be empty", Toast.LENGTH_SHORT).show();
        } else {
            addProduct();
        }

    }

    private void addProduct() {
        progressDialog.setMessage("Adding Product...");
        progressDialog.show();

        final String timestamp = "" + System.currentTimeMillis();

        if (ImageUri == null) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("productID", timestamp);
            hashMap.put("productName", Pname);
            hashMap.put("productPrice", Pprice);
            hashMap.put("productQuantity", Pquan);
            hashMap.put("productDescription", Pdesc);
            hashMap.put("productCategory", Pcategory);
            hashMap.put("productCondition", Pcond);
            hashMap.put("productIcon", "");
            hashMap.put("timestamp", "" + timestamp);
            hashMap.put("uid", "" + firebaseAuth.getUid());

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
            reference.child("Products").child(firebaseAuth.getUid()).child(timestamp).setValue(hashMap)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressDialog.dismiss();
                            Toast.makeText(addNewProductActivity.this, "Product Added..", Toast.LENGTH_SHORT).show();
                            clearData();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(addNewProductActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            String filePathAndName = "product_images" + "" + timestamp;
            FirebaseStorage storage;
            StorageReference storageReference;
            storage = FirebaseStorage.getInstance();
            storageReference = storage.getReference();

            StorageReference ref = storageReference.child("images/" + firebaseAuth.getUid() + "/" + timestamp);
            ref.putFile(ImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!uriTask.isSuccessful()) ;
                            Uri downloadImageUrl = uriTask.getResult();

                            if (uriTask.isSuccessful()) {
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put("productID", "" + timestamp);
                                hashMap.put("productName", Pname);
                                hashMap.put("productPrice", Pprice);
                                hashMap.put("productQuantity", Pquan);
                                hashMap.put("productDescription", Pdesc);
                                hashMap.put("productCategory", Pcategory);
                                hashMap.put("productCondition", Pcond);
                                hashMap.put("productIcon", "" + downloadImageUrl);
                                hashMap.put("timestamp", "" + timestamp);
                                hashMap.put("uid", "" + firebaseAuth.getUid());

                                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                                reference.child("Products").child(firebaseAuth.getUid()).child(timestamp).setValue(hashMap)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                progressDialog.dismiss();
                                                Toast.makeText(addNewProductActivity.this, "Product Added..", Toast.LENGTH_SHORT).show();
                                                clearData();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                progressDialog.dismiss();
                                                Toast.makeText(addNewProductActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(addNewProductActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
        }
    }

    private void clearData() {
        addPname.setText("");
        addPrice.setText("");
        addQuan.setText("");
        addPcat.setText("");
        addPdesc.setText("");
        addPcond.setText("");
        addProdImage.setImageResource(R.drawable.ic_add_a_photo);
        ImageUri = null;
    }


    private void categoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Product Category")
                .setItems(Constant.productCategories, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String category = Constant.productCategories[which];
                        addPcat.setText(category);
                    }
                })
                .show();
    }


    private void OpenGallery() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GalleryPick);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GalleryPick && resultCode == RESULT_OK && data != null && data.getData() != null) {
            ImageUri = data.getData();
            addProdImage.setImageURI(ImageUri);
        }
    }
}