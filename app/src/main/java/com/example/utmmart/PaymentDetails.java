package com.example.utmmart;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.utmmart.Model.ModelCartItem;
import com.example.utmmart.Model.ModelOrder;
import com.example.utmmart.Model.ModelOrderItem;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import p32929.androideasysql_library.Column;
import p32929.androideasysql_library.EasyDB;

public class PaymentDetails  extends AppCompatActivity {

    TextView txtId, txtAmount, txtStatus;
    Button btnbacktohome;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        txtId = (TextView)findViewById(R.id.txtId);
        txtAmount = (TextView)findViewById(R.id.txtAmount);
        txtStatus = (TextView)findViewById(R.id.txtStatus);
        btnbacktohome = (Button) findViewById(R.id.btnbacktohome);

        //button back to home
        btnbacktohome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentDetails.this, home.class));
            }
        });

        //Get Intent
        Intent intent = getIntent();

        try{
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonObject.getJSONObject("response"),intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        storeData();
    }

    private void showDetails(JSONObject response, String paymentAmount) {
        try {
            txtId.setText(response.getString("id"));
            // txtAmount.setText("$"+paymentAmount);
            // txtStatus.setText(response.getString("status"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void storeData(){

        firebaseAuth = FirebaseAuth.getInstance();
        String timestamp = "" + System.currentTimeMillis();

        EasyDB easyDB = EasyDB.init(this, "ITEMS_DB")
                .setTableName("ITEMS_TABLE")
                .addColumn(new Column("Item_Id", new String[]{"text", "unique"}))
                .addColumn(new Column("Item_PID", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_UID", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Name", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Price", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Quantity", new String[]{"text", "not null"}))
                .doneTableColumn();
        //get records from DB
        Cursor res = easyDB.getAllData();

        Boolean orderCreated = false;

        Double totalCost = 0.0;

        while(res.moveToNext()){
            String uid = res.getString(3);
            if(uid.equals(firebaseAuth.getUid())) {
                String price = res.getString(5);
                String quantity = res.getString(6);
                Double cost = (Double.parseDouble(price) * Integer.parseInt(quantity));
                totalCost += cost;
            }
        }

        Cursor res2 = easyDB.getAllData();

        while(res2.moveToNext()){
            String uid = res2.getString(3);
            if(uid.equals(firebaseAuth.getUid())) {
                String id = res2.getString(1);
                String pId = res2.getString(2);
                String name = res2.getString(4);
                String price = res2.getString(5);
                String quantity = res2.getString(6);
                String cost = "" + (Double.parseDouble(price) * Integer.parseInt(quantity));

                if(!orderCreated){
                    ModelOrder modelOrder = new ModelOrder(timestamp, timestamp, "In Progress", "" + totalCost, uid);

                    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                    reference.child("Orders").child(uid).child(timestamp).setValue(modelOrder)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                }
                            });

                    orderCreated = true;
                }

                ModelOrderItem modelOrderItem = new ModelOrderItem(pId, timestamp, name, cost, price, quantity);

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

                ref.child("OrderItems").child(timestamp).child(pId).setValue(modelOrderItem)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });

                easyDB.deleteRow(1,id);
            }
        }

        txtAmount.setText(txtAmount.getText() + ": $" + totalCost);
        txtStatus.setText(txtStatus.getText() + ": Success");
    }

}