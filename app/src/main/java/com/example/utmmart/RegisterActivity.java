package com.example.utmmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class RegisterActivity extends AppCompatActivity {

    TextView btn;
    private EditText inputusername,inputPassword,inputEmail,inputConfirmpassword,inputAddress;
    Button btnRegister;
    private FirebaseAuth mAuth;
    private ProgressDialog mLoadingBar;
    private FirebaseUser curentUser;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btn = findViewById(R.id.alreadyHaveAccount);
        inputusername = findViewById(R.id.inputUserName);
        inputPassword = findViewById(R.id.inputPassword);
        inputAddress = findViewById(R.id.input_Address);
        inputEmail = findViewById(R.id.inputEmail);
        inputConfirmpassword = findViewById(R.id.inputConfirmpassword);
        mAuth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        mLoadingBar = new ProgressDialog(RegisterActivity.this);

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCrededentials();
            }});

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this,
                        login.class));
            }
        });
    }

    private void checkCrededentials() {
        String username =inputusername.getText().toString();
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        String confirmPassword = inputConfirmpassword.getText().toString();
        String address = inputAddress.getText().toString();
        //masukkan data ke dalam real time database
        // reference.push().setValue(register);

        if(username.isEmpty() || username.length()<7){
            showError(inputusername,  "Your username is not valid!");
        }
        else if (email.isEmpty() || !email.contains("@") ){
            showError(inputEmail,  "Email is not valid!");
        }
        else if (password.isEmpty() || password.length()<7){
            showError(inputPassword,"Password must be 7 character!");
        }
        else if (confirmPassword.isEmpty() || !confirmPassword.equals(password)){
            showError(inputConfirmpassword, "Password not match!");
        }

        else if (address.isEmpty())
        {
            showError(inputAddress,"Please insert your Address");
        }
        else
        {
            mLoadingBar.setTitle("Registration");
            mLoadingBar.setMessage("Please Wait,while check your confidential");
            mLoadingBar.setCanceledOnTouchOutside(false);
            mLoadingBar.show();

            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){

                        //chat notification
                        // String devicetoken = FirebaseInstanceId.getInstance().getToken();
                        //automatically send to chat room :cuba jer herher

                        String curentUserID = mAuth.getCurrentUser().getUid();
                        Register register = new Register(curentUserID, "", email,username,address, "", ""," ");
                        reference.child(curentUserID).setValue(register);
                        //  reference.child("Users").child(curentUserID).child("device_token")
                        //   .setValue(devicetoken);

                        //sendUsertoMessageActivity();
                        Toast.makeText(RegisterActivity.this,"Successfully Registration",Toast.LENGTH_SHORT).show();
                        mLoadingBar.dismiss();
                        //   Intent intent = new Intent(RegisterActivity.this, MessageActivity.class);
                        //  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        // startActivity(intent);
                    }
                    else{
                        Toast.makeText(RegisterActivity.this,task.getException().toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    //dariregister to chatroom :saja jer herher
    private void sendUsertoMessageActivity(){
        Intent register = new Intent(RegisterActivity.this,MessageActivity.class);
        register.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//cannot go back
        startActivity(register);
        finish();
    }

    private void sendUsertoLoginActivity()
    {
        Intent register = new Intent(RegisterActivity.this,login.class);
        register.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//cannot go back
        startActivity(register);
        finish();
    }



    private void showError(EditText input,String s){
        input.setError(s);
        input.requestFocus();
    }
}