package com.example.utmmart.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmmart.EditProductActivity;
import com.example.utmmart.FilterProduct;
import com.example.utmmart.FilterProduct2;
import com.example.utmmart.MainProfileActivity;
import com.example.utmmart.Model.ModelProduct;
import com.example.utmmart.ProductDetailsActivity;
import com.example.utmmart.R;
import com.example.utmmart.addNewProductActivity;
import com.example.utmmart.home;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import p32929.androideasysql_library.Column;
import p32929.androideasysql_library.EasyDB;

public class AdapterProduct2 extends RecyclerView.Adapter<AdapterProduct2.HolderProduct> implements Filterable {

    private Context context;
    public ArrayList<ModelProduct> productList, filterList;
    private FilterProduct2 filter;

    public AdapterProduct2 (Context context, ArrayList<ModelProduct> productList) {
        this.context = context;
        this.productList = productList;
        this.filterList = productList;
    }

    @NonNull
    @Override
    public HolderProduct onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_product, parent, false);
        return new HolderProduct(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderProduct holder, int position) {

        ModelProduct modelProduct = productList.get(position);
        String id = modelProduct.getTimestamp();
        String uid = modelProduct.getUid();
        String ProductPrice = modelProduct.getProductPrice();
        String ProductCategory = modelProduct.getProductCategory();
        String ProductQuantity = modelProduct.getProductQuantity();
        String ProductDescription = modelProduct.getProductDescription();
        String ProductIcon = modelProduct.getProductIcon();
        String ProductName = modelProduct.getProductName();
        String timestamp = modelProduct.getTimestamp();

        holder.productName.setText(ProductName);
        holder.quantitytv.setText(ProductQuantity);
        holder.priceTv.setText("RM" + ProductPrice);

        try {
            Picasso.get().load(ProductIcon).placeholder(R.drawable.ic_shopping_cart).into(holder.productIconIv);
        } catch (Exception e) {
            holder.productIconIv.setImageResource(R.drawable.ic_shopping_cart);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //detailsBottomSheet(modelProduct);
                //startActivity(new Intent(home.this, ProductDetailsActivity.class));

                Intent intent = new Intent(context,ProductDetailsActivity.class);
                intent.putExtra("productID", id);
                intent.putExtra("userID", uid);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new FilterProduct2(AdapterProduct2.this, filterList);
        }

        return filter;
    }

    class HolderProduct extends RecyclerView.ViewHolder {

        private ImageView productIconIv, nextIv;
        private TextView productName, quantitytv, priceTv;

        public HolderProduct(@NonNull View itemView) {
            super(itemView);

            productIconIv = itemView.findViewById(R.id.productIconIv);
            nextIv = itemView.findViewById(R.id.nextIv);
            productName = itemView.findViewById(R.id.productName);
            quantitytv = itemView.findViewById(R.id.quantitytv);
            priceTv = itemView.findViewById(R.id.priceTv);
        }
    }
}