package com.example.utmmart.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmmart.Model.ModelOrder;
import com.example.utmmart.OrderDetails;
import com.example.utmmart.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class AdapterOrder extends RecyclerView.Adapter<AdapterOrder.HolderOrder> {

    private Context context;
    private ArrayList<ModelOrder> orderList;

    public AdapterOrder(Context context, ArrayList<ModelOrder> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public HolderOrder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_order, parent, false);
        return new HolderOrder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderOrder holder, int position) {

        ModelOrder modelOrder = orderList.get(position);
        String orderId = modelOrder.getOrderId();
        String orderBy = modelOrder.getOrderBy();
        String orderCost = modelOrder.getOrderCost();
        String orderStatus = modelOrder.getOrderStatus();
        String orderTime = modelOrder.getOrderTime();
        // String orderTo = modelOrder.getOrderTo();

        // loadShopInfo(modelOrder, holder);

        holder.amountTv.setText("Amount: RM" + orderCost);
        holder.statusTv.setText(orderStatus);
        holder.orderIdTv.setText("OrderId:" +orderId);
        if (orderStatus.equals("In Progress")){
            holder.statusTv.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        else if (orderStatus.equals("Complete")){
            holder.statusTv.setTextColor(context.getResources().getColor(R.color.green));
        }
        else if (orderStatus.equals("Cancelled")){
            holder.statusTv.setTextColor(context.getResources().getColor(R.color.colorRed));
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(orderTime));
        String formatedDate = DateFormat.format("dd/MM/yyyy", calendar).toString();

        holder.dateTv.setText(formatedDate);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetails.class);
                intent.putExtra("Order Id", orderId);
                // intent.putExtra("Order To", orderTo);
                context.startActivity(intent);
            }
        });

    }

//    private void loadShopInfo(ModelOrder modelOrder, HolderOrder holder) {
//        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("User");
//        ref.child(modelOrder.getOrderTo())
//                .addValueEventListener(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                        String shopName = ""+dataSnapshot.child("username").getValue();
//                        holder.shopNameTv.setText(shopName);
//
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError error) {
//
//                    }
//                });
//    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    class HolderOrder extends RecyclerView.ViewHolder{

        private TextView orderIdTv, dateTv, shopNameTv, amountTv, statusTv;

        public HolderOrder(@NonNull View itemView) {
            super(itemView);

            orderIdTv = itemView.findViewById(R.id.orderIdTv);
            dateTv = itemView.findViewById(R.id.dateTv);
            shopNameTv = itemView.findViewById(R.id.shopNameTv);
            amountTv = itemView.findViewById(R.id.amountTv);
            statusTv = itemView.findViewById(R.id.statusTv);
        }
    }
}