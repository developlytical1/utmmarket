package com.example.utmmart.Adapter;
//message
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.utmmart.Chats;
import com.example.utmmart.ContactFragments;
import com.example.utmmart.Groups_Fragment;
import com.example.utmmart.RequestMessageFragment;

public class viewPagerAdapter extends FragmentPagerAdapter {
    public viewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                Chats chatfragment = new Chats();
                return new Chats();

            case 1:
                Groups_Fragment groups_fragment = new Groups_Fragment();
                return new Groups_Fragment();


            case 2:
            ContactFragments contactFragments = new ContactFragments();
            return new ContactFragments();

          case 3:
              RequestMessageFragment requestMessageFragment = new RequestMessageFragment();
            return new RequestMessageFragment();


            default :
                return null;
        }

    }

    @Override
    public int getCount() {
        return 4;
    }

    public CharSequence getPageTitle(int position){
        switch (position){
            case 0 :return "Chats";
            case 1 :return "Groups";
            case 2 :return "Contacts";
            case 3 :return "Requests";

        }
        return null;
    }
}
