package com.example.utmmart;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class EditProductActivity extends AppCompatActivity {

    private String Pname, Pprice, Pquan, Pcond, Pcategory, Pdesc, saveCurrentDate, saveCurrentTime;
    private Button UpdatePrdbtn;
    private ImageButton Pbackbtn;
    private ImageView addProdImage;

    private EditText addPname, addPrice, addQuan, addPcond, addPcat, addPdesc;
    private static final int GalleryPick = 1;
    private Uri ImageUri;
    private String productRandomKey, downloadImageUrl;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;


    private FirebaseDatabase mDatabase;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;

    private String productId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        productId = getIntent().getStringExtra("productID");

        Pbackbtn = (ImageButton) findViewById(R.id.PbackBtn);
        UpdatePrdbtn = (Button) findViewById(R.id.UpdateProd_btn);
        addProdImage = (ImageView) findViewById(R.id.addNewProductImg);
        addPname = (EditText) findViewById(R.id.addProd_name);
        addPrice = (EditText) findViewById(R.id.addProd_price);
        addQuan = (EditText) findViewById(R.id.addProd_Quantity);
        addPcond = (EditText) findViewById(R.id.addProd_cond);
        addPcat = (EditText) findViewById(R.id.addProd_category);
        addPdesc = (EditText) findViewById(R.id.addProd_desc);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCanceledOnTouchOutside(false);

        mDatabase = FirebaseDatabase.getInstance();
        databaseReference = mDatabase.getReference();
        storageReference = FirebaseStorage.getInstance().getReference();

        firebaseAuth = FirebaseAuth.getInstance();
        loadProductDetails();

        addProdImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenGallery();
            }
        });

        Pbackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        UpdatePrdbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputData();
            }
        });

        addPcat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryDialog();
            }
        });
    }

    private void loadProductDetails() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Products").child(firebaseAuth.getUid()).child(productId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String id = ""+dataSnapshot.child("productId").getValue();
                        String productName = ""+dataSnapshot.child("productName").getValue();
                        String productDescription = ""+dataSnapshot.child("productDescription").getValue();
                        String productCategory = ""+dataSnapshot.child("productCategory").getValue();
                        String productCondition= ""+dataSnapshot.child("productCondition").getValue();
                        String productQuantity = ""+dataSnapshot.child("productQuantity").getValue();
                        String productPrice = ""+dataSnapshot.child("productPrice").getValue();
                        String productIcon = ""+dataSnapshot.child("productIcon").getValue();
                        String timestamp = ""+dataSnapshot.child("timestamp").getValue();
                        String uid = ""+dataSnapshot.child("uid").getValue();

                        addPname.setText(productName);
                        addPdesc.setText(productDescription);
                        addPcat.setText(productCategory);
                        addPcond.setText(productCondition);
                        addQuan.setText(productQuantity);
                        addPrice.setText(productPrice);

                        try{
                            Picasso.get().load(productIcon).placeholder(R.drawable.ic_add_a_photo).into(addProdImage);
                        }
                        catch (Exception e){
                            addProdImage.setImageResource(R.drawable.ic_add_a_photo);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

    }

    public void inputData() {
        Pname = addPname.getText().toString().trim();
        Pprice = addPrice.getText().toString().trim();
        Pquan = addQuan.getText().toString().trim();
        Pdesc = addPdesc.getText().toString().trim();
        Pcategory = addPcat.getText().toString().trim();
        Pcond = addPcond.getText().toString().trim();

        if (TextUtils.isEmpty(Pname)) {
            Toast.makeText(this, "Product name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(Pprice)) {
            Toast.makeText(this, "Product price cannot be empty", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pquan)) {
            Toast.makeText(this, "Product quantity cannot be empty", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pdesc)) {
            Toast.makeText(this, "Product description cannot be empty", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pcategory)) {
            Toast.makeText(this, "Please choose your product category", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(Pcond)) {
            Toast.makeText(this, "Product condition cannot be empty", Toast.LENGTH_SHORT).show();
        } else {
            updateProduct();
        }

    }

    private void updateProduct() {
        progressDialog.setMessage("Updating Product...");
        progressDialog.show();

        if(ImageUri == null){

            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("productName",""+Pname);
            hashMap.put("productDescription",""+Pdesc);
            hashMap.put("productCategory",""+ Pcategory);
            hashMap.put("productCondition",""+ Pcond);
            hashMap.put("productQuantity",""+Pquan);
            hashMap.put("productPrice",""+Pprice);

            //update to db
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
            reference.child("Products").child(firebaseAuth.getUid()).child(productId)
                    .updateChildren(hashMap)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            progressDialog.dismiss();
                            Toast.makeText(EditProductActivity.this, "Updated...", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(EditProductActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else{
            //update with image

            FirebaseStorage storage;
            StorageReference storageReference;
            storage = FirebaseStorage.getInstance();
            storageReference = storage.getReference();

            StorageReference ref = storageReference.child("images/" + firebaseAuth.getUid() + "/" + productId);
            ref.putFile(ImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!uriTask.isSuccessful());
                            Uri downloadImageUri = uriTask.getResult();

                            if (uriTask.isSuccessful()){
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put("productName",""+Pname);
                                hashMap.put("productDescription",""+Pdesc);
                                hashMap.put("productCategory",""+ Pcategory);
                                hashMap.put("productCondition",""+ Pcond);
                                hashMap.put("productQuantity",""+Pquan);
                                hashMap.put("productPrice",""+Pprice);
                                hashMap.put("productIcon", "" + downloadImageUri);

                                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                                reference.child("Products").child(firebaseAuth.getUid()).child(productId)
                                        .updateChildren(hashMap)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                progressDialog.dismiss();
                                                Toast.makeText(EditProductActivity.this, "Updated...", Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                progressDialog.dismiss();
                                                Toast.makeText(EditProductActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(EditProductActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
        }
    }


    private void categoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Product Category")
                .setItems(Constant.productCategories, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String category = Constant.productCategories[which];
                        addPcat.setText(category);
                    }
                })
                .show();
    }


    private void OpenGallery() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, GalleryPick);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GalleryPick && resultCode == RESULT_OK && data != null && data.getData() != null) {
            ImageUri = data.getData();
            addProdImage.setImageURI(ImageUri);
        }
    }

}