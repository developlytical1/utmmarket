package com.example.utmmart.Adapter;

import android.content.Context;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmmart.Model.ModelCartItem;
import com.example.utmmart.Model.ModelOrderItem;
import com.example.utmmart.R;

import java.util.ArrayList;

public class AdapterOrderedItem extends RecyclerView.Adapter<AdapterOrderedItem.HolderOrderedItem> {

    private Context context;
    private ArrayList<ModelOrderItem> orderItemArrayList;

    public AdapterOrderedItem(Context context, ArrayList<ModelOrderItem> orderItemArrayList) {
        this.context = context;
        this.orderItemArrayList = orderItemArrayList;
    }

    @NonNull
    @Override
    public HolderOrderedItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_order_item, parent, false);
        return new HolderOrderedItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderOrderedItem holder, int position) {

        ModelOrderItem modelOrderItem = orderItemArrayList.get(position);
        String getpId = modelOrderItem.getpId();
        String name = modelOrderItem.getName();
        String cost = modelOrderItem.getCost();
        String price = modelOrderItem.getPrice();
        String quantity = modelOrderItem.getQuantity();

        holder.itemTitleTv.setText(name);
        holder.itemPriceEachTv.setText("RM"+price);
        holder.itemPriceTv.setText("RM"+cost);
        holder.itemQuantityTv.setText("["+quantity+"]");

    }

    @Override
    public int getItemCount() {
        return orderItemArrayList.size();
    }


    class HolderOrderedItem extends RecyclerView.ViewHolder{

        private TextView itemTitleTv, itemPriceTv, itemPriceEachTv, itemQuantityTv;


        public HolderOrderedItem(@NonNull View itemView) {
            super(itemView);

            itemTitleTv = itemView.findViewById(R.id.itemTitleTv);
            itemPriceTv = itemView.findViewById(R.id.itemPriceTv);
            itemPriceEachTv = itemView.findViewById(R.id.itemPriceEachTv);
            itemQuantityTv = itemView.findViewById(R.id.itemQuantityTv);
        }
    }
}
