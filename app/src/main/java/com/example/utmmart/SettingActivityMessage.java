package com.example.utmmart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingActivityMessage extends AppCompatActivity {

    private Button UpdateAccountSetting;
    private EditText userName,userStatus;
    private CircleImageView userProfileImage;

    private String currentUsedId;
    private FirebaseAuth mAuth;
    private DatabaseReference reference;

    private static final int GalleryPick =1 ;
    private StorageReference UserProfileImagesReferences;
    private ProgressDialog loadinBar;

    private Toolbar SettingsToolbar ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_message);

        mAuth = FirebaseAuth.getInstance();
        currentUsedId = mAuth.getCurrentUser().getUid();
        reference = FirebaseDatabase.getInstance().getReference();
        UserProfileImagesReferences = FirebaseStorage.getInstance().getReference().child("Profile Images");
        SettingsToolbar = (Toolbar) findViewById(R.id.setting_toolbar);
        setSupportActionBar(SettingsToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setTitle("Account Setting");

       initializedFields();
       userName.setVisibility(View.INVISIBLE);

       UpdateAccountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateSetting();
            }


        });

       RetrieveUserInformation();

        //profile image
       userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent,GalleryPick);
            }
        });
    }

    private void initializedFields() {

        UpdateAccountSetting = (Button) findViewById(R.id.update_setting);
        userName = (EditText) findViewById(R.id.set_user_name);
        userStatus =(EditText) findViewById(R.id.set_profile_status);
         userProfileImage =(CircleImageView) findViewById(R.id.set_profile_image);
        loadinBar = (ProgressDialog) new ProgressDialog(this);
    }

    private void RetrieveUserInformation() {
        reference.child("Users").child(currentUsedId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists() && (snapshot.hasChild("name") && (snapshot.hasChild("image")))){
                    String retrieveUserName = snapshot.child("name").getValue().toString();
                    String retrieveStatus= snapshot.child("status").getValue().toString();
                    String retrieveProfileImage= snapshot.child("image").getValue().toString();

                    userName.setText(retrieveUserName);
                    userStatus.setText(retrieveStatus);
                    Picasso.get().load(retrieveProfileImage).into(userProfileImage);
                }

                else if(snapshot.exists() && (snapshot.hasChild("name") && (snapshot.hasChild("image")))){
                    String retrieveUserName = snapshot.child("name").getValue().toString();
                    String retrieveStatus= snapshot.child("status").getValue().toString();


                    userName.setText(retrieveUserName);
                    userStatus.setText(retrieveStatus);
                }
                else
                {
                    userName.setVisibility(View.VISIBLE);
                    Toast.makeText(SettingActivityMessage.this,"please update and set your profile information",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void UpdateSetting() {
        String setUserName = userName.getText().toString();
        String setUserStatus = userStatus.getText().toString();

        if(TextUtils.isEmpty(setUserName))
        {
            Toast.makeText(this,"please write your username",Toast.LENGTH_SHORT).show();
        }


        if(TextUtils.isEmpty(setUserStatus))
        {
            Toast.makeText(this,"please write your status",Toast.LENGTH_SHORT).show();
        }

        else
        {
            HashMap<String, Object> profileMap = new HashMap<>();
                profileMap.put("uid",currentUsedId);
                profileMap.put("name",setUserName);
                profileMap.put("status",setUserStatus);
             reference.child("Users").child(currentUsedId).setValue(profileMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                 @Override
                 public void onComplete(@NonNull Task<Void> task) {
                     if(task.isSuccessful()){
                         sendUsertoMessageActivity();
                         Toast.makeText(SettingActivityMessage.this,"Profile Updated Successfully",Toast.LENGTH_SHORT).show();
                     }
                     else
                     {
                         String message =task.getException().toString();
                         Toast.makeText(SettingActivityMessage.this,"Eror"+ message,Toast.LENGTH_SHORT).show();
                     }
                 }
             });
        }

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GalleryPick && resultCode == RESULT_OK && data!=null){
            Uri ImageUri = data.getData();
            // start picker to get image for cropping and then use the image in cropping activity
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if(requestCode == RESULT_OK){
                loadinBar.setTitle("Set Profile Image");
                loadinBar.setMessage("Please wait,you profile was updatting....");
                loadinBar.setCanceledOnTouchOutside(false);
                loadinBar.show();
                Uri resultUri = result.getUri();



                StorageReference filepath = UserProfileImagesReferences.child(currentUsedId +".jpg");
                filepath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                       if(task.isSuccessful()) {
                           Toast.makeText(SettingActivityMessage.this,"Profile Image uploaded Sucessfully",Toast.LENGTH_SHORT).show();
                           final String downloadURL = task.getResult().getMetadata().getReference().getDownloadUrl().toString();
                           reference.child("Users").child(currentUsedId).child("image")
                                   .setValue(downloadURL).addOnCompleteListener(new OnCompleteListener<Void>() {
                               @Override
                               public void onComplete(@NonNull Task<Void> task) {
                               if(task.isSuccessful()){
                                   Toast.makeText(SettingActivityMessage.this,"Image save in Database,Sucessfully ",Toast.LENGTH_SHORT).show();
                                   loadinBar.dismiss();
                               }
                               else{
                                   String message =task.getException().toString();
                                   Toast.makeText(SettingActivityMessage.this,"Error"+message,Toast.LENGTH_SHORT).show();
                                   loadinBar.dismiss();

                               }
                               }
                           });
                       }

                       else
                       {
                           String message = task.getException().toString();
                           Toast.makeText(SettingActivityMessage.this,"Eror:"+message,Toast.LENGTH_SHORT).show();
                           loadinBar.dismiss();


                       }
                    }
                });
            }
        }

    }

    private void sendUsertoMessageActivity(){
        Intent register = new Intent(SettingActivityMessage.this,MessageActivity.class);
        register.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//cannot go back
        startActivity(register);
        finish();
    }
}