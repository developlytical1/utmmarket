package com.example.utmmart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

//login with google need implements
public class login extends AppCompatActivity  {

    TextView btn,forgotPassword;
    EditText inputEmail,inputPassword;
    Button btnlogin,Google,phone;
     private FirebaseAuth mAuth;
      ProgressDialog mLoadingBar;
      //google

      private GoogleSignInClient mGoogleSignInClient;
      //private static final int SIGN_IN = 1;
    int RC_SIGN_IN=0;
   String currentUserID;
    private DatabaseReference UserRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //initialized
        btn = findViewById(R.id.textViewSignUp);
        inputEmail = findViewById(R.id.inputEmail);
        inputPassword = findViewById(R.id.inputPassword);

        //forgot password
        forgotPassword = findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(login.this,ForgotPassword.class));
            }
        });

        //forgotPassword


        btnlogin = findViewById(R.id.btnlogin);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCrededentials();
            }});
        mAuth = FirebaseAuth.getInstance();
        UserRef = FirebaseDatabase.getInstance().getReference().child("Users");
        mLoadingBar = new ProgressDialog(login.this);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(login.this,RegisterActivity.class));
            }
        });

        phone = findViewById(R.id.btnphonenumber);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent phoneloginIntent = new Intent (login.this,PhoneLoginActivity.class);
                startActivity(phoneloginIntent);
            }
        });




        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        //GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        // Build a GoogleSignInClient with the options specified by gso.
        //googleApiClient = GoogleSignIn.getClient(this, gso);
               // googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(mAuth.GOOGLE_SIGN_IN_API,gso).build();
       // GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
      //  updateUI(account);
     /* Google = findViewById(R.id.Google);
        Google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.Google:
                        signIn();
                        break;
                    // ...
                }
            }
        });*/
        // Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //action when click
       // Google.setOnClickListener(new View.OnClickListener() {
           // @Override
          //  public void onClick(View v) {
                //sambung esk
               // Intent intent = mAuth.GoogleSignInApi.getSignInIntent(googleApiClient);
              //  startActivityForResult(intent,SIGN_IN);



            }

            private void  signIn(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent,RC_SIGN_IN);
            }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
           Intent intent = new Intent("login.this,Chats.class");
           startActivity(intent);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Error", "signInResult:failed code=" + e.getStatusCode());

        }
    }

            public void onStart() {

                super.onStart();
                // Check for existing Google Sign In account, if the user is already signed in
               // the GoogleSignInAccount will be non-null.
                // Check for existing Google Sign In account, if the user is already signed in
// the GoogleSignInAccount will be non-null.
                GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
                //updateUI(account);


                  //  if (curentUser != null){
                        //register page
                      //  sendUsertoMessageActivity();

               //     }
            //    }

            //    private void sendUsertoMessageActivity() {
               //     Intent intent = new Intent(login.this,MessageActivity.class);
                //    startActivity(intent);

                }

     //   });
   // }
    private void checkCrededentials() {
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();
        if (email.isEmpty() || !email.contains("@")){
            showError(inputEmail, "Email is not valid!");
        }
        else if (password.isEmpty() || password.length()<7){
            showError(inputPassword,"Password must be 7 character!");
        }
        else
        {
            mLoadingBar.setTitle("Login");
            mLoadingBar.setMessage("Please Wait,while check your confidential");
            mLoadingBar.setCanceledOnTouchOutside(false);
            mLoadingBar.show();

            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                         currentUserID = mAuth.getCurrentUser().getUid();
                    //  String devicetoken = FirebaseInstanceId.getInstance().getToken();
                    //  UserRef.child(currentUserID).child("device_token").setValue(devicetoken)
                             //   .addOnCompleteListener(new OnCompleteListener<Void>() {
                               //     @Override

                                        //    UserRef.child(currentUserID).child().setValue()
                          /*      .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful())
                                        {
                                           // sendUsertoMessageActivity();
                                            sendUsertoHomeActivity();
                                            Toast.makeText(login.this,"Successfully Registration",Toast.LENGTH_SHORT).show();
                                            mLoadingBar.dismiss();
                                        }

                                    }
                                }); */


                       // sendUsertoMessageActivity(); //login to MessageActivity
                        sendUsertoHomeActivity();
                        Toast.makeText(login.this,"Successfully Registration",Toast.LENGTH_SHORT).show();
                        mLoadingBar.dismiss();
                        Intent intent = new Intent(login.this, home.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                    else{
                        Toast.makeText(login.this,task.getException().toString(),Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    //login to MessageActivity
    private void sendUsertoMessageActivity(){
        Intent register = new Intent(login.this,MessageActivity.class);
        register.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//cannot go back
        startActivity(register);
        finish();
    }

    //login to HomeActivity
    private void sendUsertoHomeActivity(){
       Intent home = new Intent ( login.this, home.class) ;
       home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
       startActivity(home);
       finish();
    }


    private void showError(EditText input,String s){
        input.setError(s);
        input.requestFocus();
    }

    //google Sign In
   // @Override
    //public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

   }

