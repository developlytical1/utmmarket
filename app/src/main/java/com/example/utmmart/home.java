package com.example.utmmart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmmart.Adapter.AdapterCartItem;
import com.example.utmmart.Adapter.AdapterProduct;
import com.example.utmmart.Adapter.AdapterProduct2;
import com.example.utmmart.Model.ModelCartItem;
import com.example.utmmart.Model.ModelProduct;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.v;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import p32929.androideasysql_library.Column;
import p32929.androideasysql_library.EasyDB;

public class home extends AppCompatActivity {

    private RecyclerView productsRv;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecycleViewAdapter recyclerViewAdapter;
    private ImageView chatbutton;
    private ImageButton addtochart_btn, filterProductBtn, logout2;
    private FirebaseAuth mAuth;
    private CircleImageView profile;
    private EditText search;
    private AdapterProduct2 adapterProduct2;
    private ArrayList<ModelProduct> productList;
    private FirebaseAuth firebaseAuth;


    //aina add to cart
    private ArrayList<ModelCartItem> cartItemList;
    private AdapterCartItem adapterCartItem;

    //int[] arr = {/*R.drawable.pic1,R.drawable.pic2,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,
    //        R.drawable.pic7,R.drawable.pic8,*/R.drawable.picture30, R.drawable.book, R.drawable.food2, R.drawable.drink};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        firebaseAuth = firebaseAuth.getInstance();
        // recyclerView=findViewById(R.id.recycleView);
        // layoutManager = new GridLayoutManager(this, 2);
        // recyclerView.setLayoutManager(layoutManager);
        // recyclerViewAdapter = new RecycleViewAdapter(arr);
        search = findViewById(R.id.search);
        filterProductBtn = findViewById(R.id.filterProductBtn);
        addtochart_btn = findViewById(R.id.addtochart_btn);
        productsRv = findViewById(R.id.productsRv);
        // recyclerView.setAdapter(recyclerViewAdapter);
        // recyclerView.setHasFixedSize(true);

        addtochart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aina show cart item
                showCartDialog();
            }
        });

        //muni:home---> messageActivity
        mAuth = FirebaseAuth.getInstance();
        chatbutton = (ImageView) findViewById(R.id.chat_icon);
        chatbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(home.this, MessageActivity.class));
            }
        });

        logout2 = findViewById(R.id.logout2);
        logout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent loginintent = new Intent(home.this,login.class);
                loginintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginintent);
                finish();
            }
        });

        //muni : home---> editprofile

        profile = (CircleImageView) findViewById(R.id.profile);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(home.this, MainProfileActivity.class));
            }
        });


        //farzana -->search
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    adapterProduct2.getFilter().filter(s);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // filter buttton
        filterProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(home.this);
                builder.setTitle("Choose Category")
                        .setItems(Constant.productCategories1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String selected = Constant.productCategories1[which];

                                if (selected.equals("All")) {

                                    loadAllProducts();
                                } else {
                                    loadFilterProduct(selected);
                                }

                            }
                        })
                        .show();
            }
        });

        checkUser();
        loadAllProducts();

    }

    //aina show cart items
    public double allTotalPrice = 0.00;
    public TextView sTotalTv, dFeeTv, totalTv, deliveryFee;
    private void showCartDialog() {
        //init list
        cartItemList = new ArrayList<>();

        //inflate cart layout
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_cart, null);
        TextView shopNameTv = view.findViewById(R.id.shopNameTv);
        RecyclerView cartItemsRv = view.findViewById(R.id.cartItemsRv);
        sTotalTv = view.findViewById(R.id.sTotalTv);
        //dFeeTv = view.findViewById(R.id.dFeeTv);
        totalTv = view.findViewById(R.id.totalTv);
        Button checkoutBtn = view.findViewById(R.id.checkoutBtn);

        //dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //set view to dialog
        builder.setView(view);

        EasyDB easyDB = EasyDB.init(this, "ITEMS_DB")
                .setTableName("ITEMS_TABLE")
                .addColumn(new Column("Item_Id", new String[]{"text", "unique"}))
                .addColumn(new Column("Item_PID", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_UID", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Name", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Price", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Quantity", new String[]{"text", "not null"}))
                .doneTableColumn();
        //get records from DB
        Cursor res = easyDB.getAllData();
        while(res.moveToNext()){
            String uid = res.getString(3);
            if(uid.equals(firebaseAuth.getUid())) {
                String id = res.getString(1);
                String pId = res.getString(2);
                String name = res.getString(4);
                String price = res.getString(5);
                String quantity = res.getString(6);
                Double costDbl = (Double.parseDouble(price) * Integer.parseInt(quantity));
                String cost = costDbl.toString();

                allTotalPrice = allTotalPrice + Double.parseDouble(cost);

                ModelCartItem modelCartItem = new ModelCartItem("" + id,

                        "" + pId,
                        "" + name,
                        "" + price,
                        "" + cost,
                        "" + quantity
                );
                cartItemList.add(modelCartItem);
            }
        }

        //aina setup show cart adapter
        adapterCartItem = new AdapterCartItem(this, cartItemList);
        //set to recycle view
        cartItemsRv.setAdapter(adapterCartItem);

        //dFeeTv.setText("RM"+deliveryFee);
        sTotalTv.setText("RM"+String.format("%.2f",allTotalPrice));
        totalTv.setText("RM"+(allTotalPrice));

        //show dialog
        AlertDialog dialog = builder.create();
        dialog.show();

        //reset total price on dialog dismiss
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                allTotalPrice = 0.00;
            }
        });

        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aina confirm order
                Intent intent = new Intent(home.this, pay.class);
                intent.putExtra("totalPrice", "" + allTotalPrice);
                startActivity(intent);
            }
        });

    }

    private void loadFilterProduct (String selected){
        productList = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Products");
        reference
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        productList.clear();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            for (DataSnapshot d : ds.getChildren()) {
                                String productCategory = "" + d.child("productCategory").getValue();
                                ModelProduct modelProduct = d.getValue(ModelProduct.class);
                                if(!modelProduct.getUid().equals(firebaseAuth.getUid()) && selected.equals(productCategory))
                                    productList.add(modelProduct);
                            }
                        }
                        adapterProduct2 = new AdapterProduct2(home.this, productList);

                        productsRv.setAdapter(adapterProduct2);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

    }

    private void loadAllProducts () {
        productList = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Products");
        reference
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        productList.clear();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            for (DataSnapshot d : ds.getChildren()) {
                                ModelProduct modelProduct = d.getValue(ModelProduct.class);
                                if(!modelProduct.getUid().equals(firebaseAuth.getUid()))
                                    productList.add(modelProduct);
                            }
                        }
                        adapterProduct2 = new AdapterProduct2(home.this, productList);

                        productsRv.setAdapter(adapterProduct2);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

    }

    private void checkUser () {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user == null) {
            startActivity(new Intent(home.this, login.class));
            finish();
        } else {
            loadMyInfo();
        }
    }

    private void loadMyInfo () {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.child(firebaseAuth.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String profile3 = "" + dataSnapshot.child("profile_image").getValue();

                        try {
                            Picasso.get().load(profile3).placeholder(R.drawable.icon_profile).into(profile);
                        } catch (Exception e) {
                            profile.setImageResource(R.drawable.icon_profile);
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });


    }
}