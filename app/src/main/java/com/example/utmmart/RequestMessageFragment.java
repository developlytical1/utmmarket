package com.example.utmmart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class RequestMessageFragment extends Fragment {

    private View RequestFreagmentView;
    private RecyclerView myRequestList;

    private DatabaseReference ChatRequestReference,UserRef,ContactRef;
    private FirebaseAuth mAuth;
    private String currentUserID;


    public RequestMessageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RequestFreagmentView = inflater.inflate(R.layout.fragment_request_message ,container, false);
        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        UserRef= FirebaseDatabase.getInstance().getReference().child("Users");
        ChatRequestReference = FirebaseDatabase.getInstance().getReference().child("Chat Request");
        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contacts");
        myRequestList = (RecyclerView) RequestFreagmentView.findViewById(R.id.chat_requests_list);
        myRequestList.setLayoutManager(new LinearLayoutManager(getContext()));
        return RequestFreagmentView;
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<Contacts> options = new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(ChatRequestReference.child(currentUserID), Contacts.class).build();

        FirebaseRecyclerAdapter<Contacts, RequestViewHolder> adapter = new FirebaseRecyclerAdapter<Contacts, RequestViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull RequestViewHolder requestViewHolder, int i, @NonNull Contacts contacts) {
                requestViewHolder.itemView.findViewById(R.id.request_accept_button).setVisibility(View.VISIBLE);
                requestViewHolder.itemView.findViewById(R.id.request_cancel_button).setVisibility(View.VISIBLE);

                final String List_user_id = getRef(i).getKey();
                DatabaseReference getTypeRef = getRef(i).child("request_type").getRef();
                getTypeRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            String type = snapshot.getValue().toString();
                            if(type.equals("received")){
                                UserRef.child(List_user_id).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                       if(snapshot.hasChild("profile_image")){
                                            //  final String requestUsername = snapshot.child("name").getValue().toString();
                                            //final String requestStatus = snapshot.child("status").getValue().toString();
                                            final String requestProfileImage = snapshot.child("profile_image").getValue().toString();

                                            //requestViewHolder.userName.setText("requestUserName");
                                            //requestViewHolder.userStatus.setText("requestUserStatus");
                                            if (requestProfileImage.isEmpty())
                                                Picasso.get().load(R.drawable.profile_message).into(requestViewHolder.profileImage);
                                           else
                                                Picasso.get().load(requestProfileImage).into(requestViewHolder.profileImage);
                                      }
                                      /*  else
                                        {
                                            final String requestUsername = snapshot.child("name").getValue().toString();
                                            final String requestStatus = snapshot.child("status").getValue().toString();

                                            requestViewHolder.userName.setText("requestUserName");
                                            requestViewHolder.userStatus.setText("requestUserStatus");
                                        }*/

                                        final String requestUsername = snapshot.child("username").getValue().toString();
                                        final String requestUserStatus = snapshot.child("status").getValue().toString();
                                        requestViewHolder.userName.setText(requestUsername);
                                        requestViewHolder.userStatus.setText("wants to connect with you");


                                        requestViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                CharSequence option[] = new CharSequence[]{
                                                        "Accept",
                                                        "Cancelled"
                                                };

                                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                builder.setTitle(requestUsername+"Chat Request");
                                                builder.setItems(option, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if(which==0) {
                                                            ContactRef.child(currentUserID).child(List_user_id).child("Contacts")
                                                                    .setValue("Saved").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if(task.isSuccessful())
                                                                    {
                                                                        ContactRef.child(List_user_id).child(currentUserID).child("Contact")
                                                                                .setValue("Saved").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if(task.isSuccessful())
                                                                                {
                                                                                    ChatRequestReference.child(currentUserID).child(List_user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                        @Override
                                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                                            if(task.isSuccessful()){
                                                                                                ChatRequestReference.child(List_user_id).child(currentUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                    @Override
                                                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                                                        if(task.isSuccessful()){

                                                                                                            Toast.makeText(getContext(),"New Contact Saved",Toast.LENGTH_SHORT).show();
                                                                                                        } }});

                                                                                            }
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });

                                                        }

                                                        if(which==1){
                                                            ChatRequestReference.child(currentUserID).child(List_user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if(task.isSuccessful()){
                                                                        ChatRequestReference.child(List_user_id).child(currentUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if(task.isSuccessful()){

                                                                                    Toast.makeText(getContext(),"Contact Deleted",Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            }
                                                                        });

                                                                    }
                                                                }
                                                            });
                                                        }

                                                    }
                                                });

                                                builder.show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                            }

                            else if (type.equals("sent")){
                                Button request_sent_btn = requestViewHolder.itemView.findViewById(R.id.request_accept_button);
                                request_sent_btn.setText("Request Sent");

                                requestViewHolder.itemView.findViewById(R.id.request_cancel_button).setVisibility(View.INVISIBLE);

                                UserRef.child(List_user_id).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if(snapshot.hasChild("profile_image")){
                                            //  final String requestUsername = snapshot.child("name").getValue().toString();
                                            //final String requestStatus = snapshot.child("status").getValue().toString();
                                            final String requestProfileImage = snapshot.child("profile_image").getValue().toString();

                                            //requestViewHolder.userName.setText("requestUserName");
                                            //requestViewHolder.userStatus.setText("requestUserStatus");
                                           if (requestProfileImage.isEmpty())
                                                Picasso.get().load(R.drawable.profile_message).into(requestViewHolder.profileImage);
                                           else
                                              Picasso.get().load(requestProfileImage).into(requestViewHolder.profileImage);

                                        }
                                      /*  else
                                        {
                                            final String requestUsername = snapshot.child("name").getValue().toString();
                                            final String requestStatus = snapshot.child("status").getValue().toString();

                                            requestViewHolder.userName.setText("requestUserName");
                                            requestViewHolder.userStatus.setText("requestUserStatus");
                                        }*/

                                        final String requestUsername = snapshot.child("username").getValue().toString();
                                        final String requestStatus = snapshot.child("status").getValue().toString();
                                        requestViewHolder.userName.setText(requestUsername);
                                        requestViewHolder.userStatus.setText("you have a sent request to " + requestUsername);


                                        requestViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                CharSequence option[] = new CharSequence[]{

                                                        "Cancel Chat Request "
                                                };

                                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                builder.setTitle("Already Sent Request");
                                                builder.setItems(option, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        if(which==0){
                                                            ChatRequestReference.child(currentUserID).child(List_user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if(task.isSuccessful()){
                                                                        ChatRequestReference.child(List_user_id).child(currentUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if(task.isSuccessful()){

                                                                                    Toast.makeText(getContext(),"you have cancelled the chat request",Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            }
                                                                        });

                                                                    }
                                                                }
                                                            });
                                                        }

                                                    }
                                                });

                                                builder.show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }

                });



            }

            @NonNull
            @Override
            public RequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_display_layout_message, parent, false);
                RequestViewHolder holder = new RequestViewHolder(view);
                return holder;
            }
        };

        myRequestList.setAdapter(adapter);
        adapter.startListening();
    }


    public static class RequestViewHolder extends  RecyclerView.ViewHolder
    {
        TextView userName, userStatus;
        CircleImageView profileImage;
        Button AcceptButton ,CancelledButton;

        public RequestViewHolder(@NonNull View itemView)
        {
            super(itemView);

            //user_display_layout_message
            userName = itemView.findViewById(R.id.user_profile_name);
            userStatus = itemView.findViewById(R.id.user_status);
            profileImage = itemView.findViewById(R.id.users_profile_image_message);
            AcceptButton = itemView.findViewById(R.id.request_accept_button);
            CancelledButton = itemView.findViewById(R.id.request_cancel_button);

        }
    }
}