package com.example.utmmart;

import android.widget.Filter;

import com.example.utmmart.Adapter.AdapterProduct;
import com.example.utmmart.Adapter.AdapterProduct2;
import com.example.utmmart.Model.ModelProduct;

import java.util.ArrayList;

public class FilterProduct2 extends Filter {

    private AdapterProduct2 adapter;
    private ArrayList<ModelProduct> filterList;


    public FilterProduct2 (AdapterProduct2 adapter, ArrayList<ModelProduct> filterList){
        this.adapter = adapter;
        this.filterList = filterList;
    }


    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();

        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();

            ArrayList<ModelProduct> filterModels = new ArrayList<>();
            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getProductName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getProductCategory().toUpperCase().contains(constraint) )
                {
                    filterModels.add(filterList.get(i));
                }
            }

            results.count = filterModels.size();
            results.values = filterModels;
        }
        else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.productList = (ArrayList<ModelProduct>) results.values;

        adapter.notifyDataSetChanged();

    }
}
