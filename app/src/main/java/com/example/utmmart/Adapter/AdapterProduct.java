package com.example.utmmart.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.utmmart.EditProductActivity;
import com.example.utmmart.FilterProduct;
import com.example.utmmart.Model.ModelProduct;
import com.example.utmmart.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.HolderProduct> implements Filterable {

    private Context context;
    public ArrayList<ModelProduct> productList, filterList;
    private FilterProduct filter;

    public AdapterProduct(Context context, ArrayList<ModelProduct> productList) {
        this.context = context;
        this.productList = productList;
        this.filterList = productList;
    }

    @NonNull
    @Override
    public HolderProduct onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_product, parent, false);
        return new HolderProduct(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderProduct holder, int position) {

        ModelProduct modelProduct = productList.get(position);
        String id = modelProduct.getProductId();
        String uid = modelProduct.getUid();
        String ProductPrice = modelProduct.getProductPrice();
        String ProductCategory = modelProduct.getProductCategory();
        String ProductQuantity = modelProduct.getProductQuantity();
        String ProductDescription = modelProduct.getProductDescription();
        String ProductIcon = modelProduct.getProductIcon();
        String ProductName = modelProduct.getProductName();
        String timestamp = modelProduct.getTimestamp();

        holder.productName.setText(ProductName);
        holder.quantitytv.setText(ProductQuantity);
        holder.priceTv.setText("RM" + ProductPrice);

        try {
            Picasso.get().load(ProductIcon).placeholder(R.drawable.ic_shopping_cart).into(holder.productIconIv);
        } catch (Exception e) {
            holder.productIconIv.setImageResource(R.drawable.ic_shopping_cart);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailsBottomSheet(modelProduct);
            }
        });

    }

    private void detailsBottomSheet(ModelProduct modelProduct) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
        View view = LayoutInflater.from(context).inflate(R.layout.bs_product_details, null);
        bottomSheetDialog.setContentView(view);

        ImageButton backBtn = view.findViewById(R.id.backBtn);
        ImageButton deleteBtn = view.findViewById(R.id.deleteBtn);
        ImageButton editBtn = view.findViewById(R.id.editBtn);
        ImageView productIconIv = view.findViewById(R.id.productIconIv);
        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView descriptionTv = view.findViewById(R.id.descriptionTv);
        TextView categoryTv = view.findViewById(R.id.categoryTv);
        TextView priceTv = view.findViewById(R.id.priceTv);

        //get data
        String id = modelProduct.getTimestamp();
        String uid = modelProduct.getUid();
        String ProductPrice = modelProduct.getProductPrice();
        String ProductCategory = modelProduct.getProductCategory();
        String ProductDescription = modelProduct.getProductDescription();
        String ProductIcon = modelProduct.getProductIcon();
        String ProductName = modelProduct.getProductName();
        String timestamp = modelProduct.getTimestamp();

        //set data
        titleTv.setText(ProductName);
        descriptionTv.setText(ProductDescription);
        categoryTv.setText(ProductCategory);
        priceTv.setText("RM" + ProductPrice);

        try {
            Picasso.get().load(ProductIcon).placeholder(R.drawable.ic_shopping_cart).into(productIconIv);
        } catch (Exception e) {
            productIconIv.setImageResource(R.drawable.ic_shopping_cart);
        }

        //show dialog
        bottomSheetDialog.show();

        //click edit
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                Intent intent = new Intent(context, EditProductActivity.class);
                intent.putExtra("productID", id);
                context.startActivity(intent);

            }
        });

        //click delete
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete")
                        .setMessage("Are you sure you want to delete this product?")
                        .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //delete
                                deleteProduct(id);

                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
            }
        });

        //click back
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

    }

    private void deleteProduct(String id) {

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Products").child(firebaseAuth.getUid()).child(id).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Product deleted...", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new FilterProduct(this, filterList);
        }

        return filter;
    }

    class HolderProduct extends RecyclerView.ViewHolder {

        private ImageView productIconIv, nextIv;
        private TextView productName, quantitytv, priceTv;


        public HolderProduct(@NonNull View itemView) {
            super(itemView);

            productIconIv = itemView.findViewById(R.id.productIconIv);
            nextIv = itemView.findViewById(R.id.nextIv);
            productName = itemView.findViewById(R.id.productName);
            quantitytv = itemView.findViewById(R.id.quantitytv);
            priceTv = itemView.findViewById(R.id.priceTv);


        }
    }
}