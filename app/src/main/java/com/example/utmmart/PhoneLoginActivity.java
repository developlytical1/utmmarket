package com.example.utmmart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class PhoneLoginActivity extends AppCompatActivity {

    private Button SendVerificationButton, verifyButton;
    private EditText InputPhoneNumber, InputVerificationCode;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private String mVerificationId,verificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private FirebaseAuth mAuth;

    private ProgressDialog mloadingbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);

        mAuth = FirebaseAuth.getInstance();
        SendVerificationButton = (Button) findViewById(R.id.send_ver_code_button);
        verifyButton = (Button) findViewById(R.id.verify_button);
        InputPhoneNumber = (EditText) findViewById(R.id.phone_number_input);
        InputVerificationCode = (EditText) findViewById(R.id.verification_code_input);
        mloadingbar = new ProgressDialog(this);

        SendVerificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = InputPhoneNumber.getText().toString();
                if (TextUtils.isEmpty(phoneNumber)) {
                    Toast.makeText(PhoneLoginActivity.this, "Please enter your number first..", Toast.LENGTH_SHORT).show();

                } else {
                    mloadingbar.setTitle("Phone Verification");
                    mloadingbar.setMessage("please wait,while we autheticating your phone");
                    mloadingbar.setCanceledOnTouchOutside(false);
                    mloadingbar.show();
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            phoneNumber,    // Phone number to verify
                            60, // Timeout and unit
                            TimeUnit.SECONDS,   //unit of tiemout
                            PhoneLoginActivity.this,             // Activity (for callback binding)
                            callbacks);         // OnVerificationStateChangedCallbacks

                }
            }
        });

        verifyButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                SendVerificationButton.setVisibility(View.VISIBLE);
                                                InputPhoneNumber.setVisibility(View.VISIBLE);

                                                String verificationCode = InputVerificationCode.getText().toString();
                                                if (TextUtils.isEmpty(verificationCode)) {
                                                    Toast.makeText(PhoneLoginActivity.this, "please write verification code", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    mloadingbar.setTitle(" Verification Code");
                                                    mloadingbar.setMessage("please wait,while we verifying code");
                                                    mloadingbar.setCanceledOnTouchOutside(false);
                                                    mloadingbar.show();

                                                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                                                    signInWithPhoneAuthCredential(credential);
                                                }
                                            }
                                        });

        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                mloadingbar.dismiss();
                Toast.makeText(PhoneLoginActivity.this, "Invalid Phone Number ,please enter with your correct entry code", Toast.LENGTH_SHORT).show();
                SendVerificationButton.setVisibility(View.VISIBLE);
                InputPhoneNumber.setVisibility(View.VISIBLE);

                verifyButton.setVisibility(View.INVISIBLE);
                InputVerificationCode.setVisibility(View.INVISIBLE);
            }

            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                Toast.makeText(PhoneLoginActivity.this,"Code has been sent",Toast.LENGTH_SHORT).show();

                SendVerificationButton.setVisibility(View.VISIBLE);
                InputPhoneNumber.setVisibility(View.VISIBLE);

                verifyButton.setVisibility(View.VISIBLE);
                InputVerificationCode.setVisibility(View.VISIBLE);

            }
        };


    }




    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            mloadingbar.dismiss();
                            Toast.makeText(PhoneLoginActivity.this,"Congratulations ,you're logged sucessfully",Toast.LENGTH_SHORT).show();
                            SendUsertoMessageActivity();
                        } else {
                                String message  = task.getException().toString();
                                Toast.makeText(PhoneLoginActivity.this,"Eror:" + message,Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void SendUsertoMessageActivity() {
        Intent messageIntent = new Intent(PhoneLoginActivity.this,home.class);
        startActivity(messageIntent);
        finish();
    }
}