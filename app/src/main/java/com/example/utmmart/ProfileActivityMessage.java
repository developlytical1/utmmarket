package com.example.utmmart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivityMessage extends AppCompatActivity {

    private String receiverUserID, senderUserID, current_State, sendUserID;
    private CircleImageView userProfileImage;
    private TextView userProfileName, userProfileStatus;
    private Button sendMessageRequestButton ,DeclineMessageRequestButton;
//databaseReferece
    private DatabaseReference UserRef,ChatRequestRef,ContactRef;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_message);

        mAuth = FirebaseAuth.getInstance();
        sendUserID = mAuth.getCurrentUser().getUid();
        UserRef = FirebaseDatabase.getInstance().getReference().child("Users");
        ChatRequestRef =FirebaseDatabase.getInstance().getReference().child("Chat Request");
        ContactRef =FirebaseDatabase.getInstance().getReference().child("Contacts");

        receiverUserID = getIntent().getExtras().get("visit_user_id").toString();
        senderUserID = mAuth.getCurrentUser().getUid();

        userProfileImage = (CircleImageView) findViewById(R.id.visit_profile_image);
        userProfileName = (TextView) findViewById(R.id.visit_user_name);
        userProfileStatus = (TextView) findViewById(R.id.visit_profile_status);
        sendMessageRequestButton = (Button) findViewById(R.id.send_message_request_button);
        DeclineMessageRequestButton= (Button) findViewById(R.id.decline_message_request_button);
        current_State = "new";

      RetrieveUserInfo();
    }

    //display and Retrieve info
    private void RetrieveUserInfo() {
        UserRef.child(receiverUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if ((snapshot.exists()) && (snapshot.hasChild("profile_image"))) {
                    String userImage = snapshot.child("profile_image").getValue().toString();
                    String userName = snapshot.child("username").getValue().toString();
                    String userStatus = snapshot.child("status").getValue().toString();

                    Picasso.get().load(userImage).placeholder(R.drawable.profile_message).into(userProfileImage);
                    userProfileName.setText(userName);
                    userProfileStatus.setText(userStatus);

                    ManageChatRequests();
                } else {
                    String userName = snapshot.child("username").getValue().toString();
                    String userStatus = snapshot.child("status").getValue().toString();

                    userProfileName.setText(userName);
                    userProfileStatus.setText(userStatus);

                    ManageChatRequests();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void ManageChatRequests() {
        ChatRequestRef.child(senderUserID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
              if(snapshot.hasChild(receiverUserID)){
                  String request_type = snapshot.child(receiverUserID).child("request_type").getValue().toString();

                  if(request_type.equals("sent")){
                      current_State = "request_sent";
                      sendMessageRequestButton.setText("Cancelled_Chat_Request");

                  }
                  else if ( request_type.equals("received")){
                      current_State = "request_received";
                      sendMessageRequestButton.setText("Accept Chat Request");
                      DeclineMessageRequestButton.setVisibility(View.VISIBLE);
                      DeclineMessageRequestButton.setEnabled(true);

                      DeclineMessageRequestButton.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {
                              CancelChatRequest();
                          }
                      });
                  }
                  else{
                      ContactRef.child(senderUserID).addListenerForSingleValueEvent(new ValueEventListener() {
                          @Override
                          public void onDataChange(@NonNull DataSnapshot snapshot) {
                              if(snapshot.hasChild(receiverUserID)){
                                  current_State = "friends";
                                  sendMessageRequestButton.setText("Remove this Contact");
                              }
                          }

                          @Override
                          public void onCancelled(@NonNull DatabaseError error) {

                          }
                      });
                  }
                  }
              }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        if (!senderUserID.equals(receiverUserID)) {

            sendMessageRequestButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendMessageRequestButton.setEnabled(false);
                    if(current_State.equals("new")){
                        SendChatReques();
                    }

                    if(current_State.equals("request_sent")){
                        CancelChatRequest();
                    }
                    if(current_State.equals("request_received")){
                        AcceptChatRequest();
                    }

                    if(current_State.equals("friends")){
                        RemoveSpecificContact();
                    }
                }
            });

        } else {
            sendMessageRequestButton.setVisibility(View.INVISIBLE);
        }
        {
        }


    }

    private void RemoveSpecificContact() {
        ContactRef.child(senderUserID).child(receiverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    ContactRef.child(receiverUserID).child(senderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful())
                            {
                                sendMessageRequestButton.setEnabled(true);
                                current_State = "new";
                                sendMessageRequestButton.setText("Send Message");

                                DeclineMessageRequestButton.setVisibility(View.INVISIBLE);
                                DeclineMessageRequestButton.setEnabled(false);
                            }
                        }
                    });
                }
            }
        });

    }

    private void AcceptChatRequest() {

        ContactRef.child(senderUserID).child(receiverUserID).child("Contacts").setValue("Saved")
        .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                      if(task.isSuccessful()){
                          ContactRef.child(receiverUserID).child(senderUserID).child("Contacts").setValue("Saved").addOnCompleteListener(new OnCompleteListener<Void>() {
                              @Override
                              public void onComplete(@NonNull Task<Void> task) {
                                  if (task.isSuccessful()) {
                                      ChatRequestRef.child(senderUserID).child(receiverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                          @Override
                                          public void onComplete(@NonNull Task<Void> task) {
                                             if(task.isSuccessful()) {
                                                 ChatRequestRef.child(receiverUserID).child(senderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                     @Override
                                                     public void onComplete(@NonNull Task<Void> task) {
                                                        sendMessageRequestButton.setEnabled(true);
                                                        current_State = "friends";
                                                        sendMessageRequestButton.setText("Remove this Contact");
                                                        DeclineMessageRequestButton.setVisibility(View.INVISIBLE);
                                                        DeclineMessageRequestButton.setEnabled(false);
                                                     }
                                                 });
                                             }
                                          }
                                      });
                                  }
                              }
                          });

                      }

                    }
                });


    }

    private void CancelChatRequest() {

        ChatRequestRef.child(senderUserID).child(receiverUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    ChatRequestRef.child(receiverUserID).child(senderUserID).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful())
                            {
                                sendMessageRequestButton.setEnabled(true);
                                current_State = "new";
                                sendMessageRequestButton.setText("Send Message");

                                DeclineMessageRequestButton.setVisibility(View.INVISIBLE);
                                DeclineMessageRequestButton.setEnabled(false);
                            }
                        }
                    });
                }
            }
        });
    }

    private void SendChatReques() {

        ChatRequestRef.child(senderUserID).child(receiverUserID).child("request_type").setValue("sent")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                      if(task.isSuccessful())  {
                          ChatRequestRef.child(receiverUserID).child(senderUserID)
                                  .child("request_type").setValue("received")
                          .addOnCompleteListener(new OnCompleteListener<Void>() {
                              @Override
                              public void onComplete(@NonNull Task<Void> task) {
                                 // if (task.isSuccessful()){
                                  //    HashMap<String,String> chatnotificationMap = new HashMap<>();
                                  //    chatnotificationMap.put("from",senderUserID);
                                 //     chatnotificationMap.put("type", "request");

                                    //  NotificationRef.child(receiverUserID).push()
                                         //     .setValue(chatnotificationMap)
                                        //      .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        //          @Override
                                              //    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful())
                                                        {
                                                            sendMessageRequestButton.setEnabled(true);
                                                            current_State ="request_sent";
                                                            sendMessageRequestButton.setText("Cancelled Chat Request");
                                                        }
                                                  }
                                              });


                                  }
                              }
                          });

                      }
                    }
            //    });

  //  }
//}