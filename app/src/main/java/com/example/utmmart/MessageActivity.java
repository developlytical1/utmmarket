package com.example.utmmart;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.example.utmmart.Adapter.viewPagerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class MessageActivity extends AppCompatActivity {

    private DatabaseReference myDatabase;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private viewPagerAdapter viewPagerAdapter;
    private FirebaseAuth mAuth;
    private DatabaseReference reference;
    private String currentUserID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.viewPager);
        toolbar = findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);

        FragmentManager manager=this.getSupportFragmentManager();
        getSupportActionBar().setTitle("Chat Room");
        viewPagerAdapter = new viewPagerAdapter(getSupportFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(viewPagerAdapter);
        reference = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.option_menuchat, menu);
        return true;
    }


    public void onStart() {

        super.onStart();

        FirebaseUser curentUser = mAuth.getCurrentUser();
        if (curentUser == null){
            //register page
            sendUsertologinActivity();
        }
        else {
            UpdateStatus("online");
            verifyUserExistence();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseUser curentUser = mAuth.getCurrentUser();
        if(curentUser != null){
            UpdateStatus("offline");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FirebaseUser curentUser = mAuth.getCurrentUser();
        if(curentUser != null){
            UpdateStatus("offline");
        }
    }

    private void verifyUserExistence() {
        String currentUserId = mAuth.getCurrentUser().getUid();
        reference.child("Users").child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            //check samada dah ada belum
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if((snapshot.child("username").exists()))
                {
                    Toast.makeText(MessageActivity.this, "Welcome", Toast.LENGTH_SHORT).show();
                }

                else{
                    sendUsertoSettingActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


    //option_menuchat


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId()== R.id.mainchat_logout){
            UpdateStatus("offline");
            mAuth.signOut();
            sendUsertologinActivity();
        }

        // if(item.getItemId()== R.id.mainchat_setting){
        //   sendUsertoSettingActivity();
        // }
        if(item.getItemId()== R.id.mainchat_create_group_option){
            RequestNewGroup();
        }
        if(item.getItemId()== R.id.mainchat_friends){
            sendUsertoFindFriendActivity();
        }

        return true;
    }

    private void RequestNewGroup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MessageActivity.this,R.style.AlertDialog);
        builder.setTitle("Enter Group Name");

        final EditText groupNamefield = new EditText(MessageActivity.this);
        groupNamefield.setHint("e.g DINDA ");
        builder.setView(groupNamefield);

        builder.setPositiveButton("Create ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String groupName = groupNamefield.getText().toString();

                if(TextUtils.isEmpty(groupName)){
                    Toast.makeText(MessageActivity.this,"Please write group Name",Toast.LENGTH_SHORT).show();
                }

                else{
                    createNewGroup(groupName);
                }
            }
        });


        builder.setNegativeButton("Cancelled ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        });

        builder.show();
    }

    private void createNewGroup(String groupName) {
        reference.child("Groups").child(groupName).setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(MessageActivity.this,groupName +"is Created Successfully",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendUsertologinActivity() {
        Intent loginintent = new Intent(MessageActivity.this,login.class);
        loginintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginintent);
        finish();
    }

    private void sendUsertoSettingActivity() {
        Intent settingintent = new Intent(MessageActivity.this,SettingActivityMessage.class);
        startActivity(settingintent);

    }

    private void sendUsertoFindFriendActivity() {
        Intent findFriendIntent = new Intent(MessageActivity.this,FindFriendsActivity.class);
        startActivity(findFriendIntent);
    }

    private void UpdateStatus(String state){
        String saveCurrentTime ,saveCurrentDate ;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
        saveCurrentTime = currentTime.format(calendar.getTime());

        HashMap<String, Object> onlineStateMap = new HashMap<>();
        onlineStateMap.put("time",saveCurrentTime);
        onlineStateMap.put("date",saveCurrentDate);
        onlineStateMap.put("state",state);

        //get Current User id
        currentUserID = mAuth.getCurrentUser().getUid();
        reference.child("Users").child(currentUserID).child("userState")
                .updateChildren(onlineStateMap);


    }
}