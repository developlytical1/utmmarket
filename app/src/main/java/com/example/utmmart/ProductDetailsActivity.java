package com.example.utmmart;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.utmmart.Adapter.AdapterCartItem;
import com.example.utmmart.Model.ModelCartItem;
import com.example.utmmart.Model.ModelProduct;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import p32929.androideasysql_library.Column;
import p32929.androideasysql_library.EasyDB;

public class ProductDetailsActivity extends AppCompatActivity {

    private ImageView productIconIv;
    private TextView price_product, product_nameET, product_detailsET, product_cET,
            product_coET;
    private TextView product_name, product_desc, product_category, product_condition, title_bar;
    private Button btn_add_to_chart, btn_buy_now, btnCancel;
    private String productId, userId;


    private static final int STORAGE_REQUEST_CODE = 300;
    private String[] storagePermissions;

    private Uri image_uri;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private ModelProduct modelProduct;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        productId = getIntent().getStringExtra("productID");
        userId = getIntent().getStringExtra("userID");

        productIconIv = findViewById(R.id.productIconIv);
        title_bar = findViewById(R.id.title_bar);
        price_product = findViewById(R.id.price_product);
        product_nameET = findViewById(R.id.product_nameET);
        product_detailsET = findViewById(R.id.product_detailsET);
        product_cET = findViewById(R.id.product_cET);
        product_coET = findViewById(R.id.product_coET);
        product_name = findViewById(R.id.product_name);
        product_desc = findViewById(R.id.product_desc);
        product_category = findViewById(R.id.product_category);
        product_condition = findViewById(R.id.product_condition);
        btn_add_to_chart = findViewById(R.id.btn_add_to_chart);
        btn_buy_now = findViewById(R.id.btn_buy_now);
        btnCancel = findViewById(R.id.btnCancel);

        storagePermissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        firebaseAuth = FirebaseAuth.getInstance();
        checkUser();


        //aina add add to cart
        btn_add_to_chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aina add product to cart
                showCartQuantityDialog(modelProduct);
            }
        });

        btn_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductDetailsActivity.this, pay.class));
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void checkUser() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user == null){
            startActivity(new Intent(ProductDetailsActivity.this, login.class));
            finish();
        }
        else {
            loadProductInfo();
        }
    }

    private void loadProductInfo() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.child("Products").child(userId).child(productId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //for (DataSnapshot ds: dataSnapshot.getChildren()){
                        String product_name2 = ""+dataSnapshot.child("productName").getValue();
                        String product_details2 = ""+dataSnapshot.child("productDescription").getValue();
                        String product_category2 = ""+dataSnapshot.child("productCategory").getValue();
                        String product_condition2 = ""+dataSnapshot.child("productCondition").getValue();
                        String uid = ""+dataSnapshot.child("uid").getValue();
                        String price_product2 = ""+dataSnapshot.child("productPrice").getValue();
                        String timestamp = ""+dataSnapshot.child("timestamp").getValue();
                        String product_icon = ""+dataSnapshot.child("productIcon").getValue();

                        product_nameET.setText(product_name2);
                        product_detailsET.setText(product_details2);
                        product_cET.setText(product_category2);
                        product_coET.setText(product_condition2);
                        price_product.setText("RM"+price_product2);

                        try {
                            Picasso.get().load(product_icon).placeholder(R.drawable.ic_shopping_cart).into(productIconIv);
                        }
                        catch (Exception e){
                            productIconIv.setImageResource(R.drawable.ic_shopping_cart);
                        }

                        modelProduct = new ModelProduct(dataSnapshot.child("productID").getValue().toString(), product_name2, product_details2, product_category2, dataSnapshot.child("productQuantity").getValue().toString(),
                                price_product2, product_icon, timestamp, uid);

                        //}
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

    }

    //aina add to cart
    private double cost = 0;
    private double finalCost = 0;
    private int quantity = 0;
    private void showCartQuantityDialog(ModelProduct modelProduct) {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_cart_quantity, null);

        ImageView productIv = view.findViewById(R.id.productIv);
        TextView titleTv = view.findViewById(R.id.titleTv);
        TextView pQuantityTv = view.findViewById(R.id.pQuantityTv);
        TextView descriptionTv = view.findViewById(R.id.descriptionTv);
        TextView priceTv = view.findViewById(R.id.priceTv);
        TextView finalTv = view.findViewById(R.id.finalTv);
        ImageButton decrementBtn = view.findViewById(R.id.decrementBtn);
        TextView quantityTv = view.findViewById(R.id.quantitytv);
        ImageButton incrementBtn = view.findViewById(R.id.incrementBtn);
        Button continueBtn = view.findViewById(R.id.continueBtn);

        //get data from model Product
        String productId = modelProduct.getProductId();
        String productName = modelProduct.getProductName();
        String productQuantity = modelProduct.getProductQuantity();
        String productDescription = modelProduct.getProductDescription();
        String image = modelProduct.getProductIcon();
        String productPrice = modelProduct.getProductPrice();

        cost = Double.parseDouble(productPrice.replaceAll("RM",""));
        finalCost = Double.parseDouble((productPrice.replaceAll("RM", "")));
        quantity = 1;

        //alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);

        //set add to cart data
        try {
            Picasso.get().load(image).placeholder(R.drawable.ic_shopping_cart).into(productIv);
        }
        catch (Exception e){
            productIv.setImageResource(R.drawable.ic_shopping_cart);
        }

        titleTv.setText(""+productName);
        pQuantityTv.setText(""+productQuantity);
        descriptionTv.setText(""+productDescription);
        priceTv.setText(""+finalCost);
        finalTv.setText("RM"+finalCost);
        quantityTv.setText(""+quantity);

        AlertDialog dialog = builder.create();
        dialog.show();

        //increment product quantity
        incrementBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalCost = finalCost + cost;
                quantity++;

                finalTv.setText("RM"+finalCost);
                quantityTv.setText(""+quantity);
            }
        });
        //decrement product quantity
        decrementBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quantity > 1){
                    finalCost = finalCost - cost;
                    quantity--;

                    finalTv.setText("RM"+finalCost);
                    quantityTv.setText(""+quantity);
                }
            }
        });
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = titleTv.getText().toString().trim();
                String price = finalTv.getText().toString().trim().replace("RM","");
                String quantity = quantityTv.getText().toString().trim();

                //add to db(SQLite)
                addToCart(productId, title, productPrice, quantity);

                dialog.dismiss();
            }
        });
    }


    private void addToCart(String productId, String title, String price, String quantity) {
        String itemId = "" + System.currentTimeMillis();

        EasyDB easyDB = EasyDB.init(this, "ITEMS_DB")
                .setTableName("ITEMS_TABLE")
                .addColumn(new Column("Item_Id",new String[]{"text", "unique"}))
                .addColumn(new Column("Item_PID", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_UID", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Name", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Price", new String[]{"text", "not null"}))
                .addColumn(new Column("Item_Quantity", new String[]{"text", "not null"}))
                .doneTableColumn();

        Boolean b = easyDB.addData("Item_Id", itemId)
                .addData("Item_PID", productId)
                .addData("Item_UID", firebaseAuth.getUid())
                .addData("Item_Name", title)
                .addData("Item_Price", price)
                .addData("Item_Quantity", quantity)
                .doneDataAdding();
        Toast.makeText(this,"Added to cart...", Toast.LENGTH_SHORT).show();

    }

}