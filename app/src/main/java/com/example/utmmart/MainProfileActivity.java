package com.example.utmmart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.utmmart.Adapter.AdapterOrder;
import com.example.utmmart.Adapter.AdapterProduct;
import com.example.utmmart.Model.ModelOrder;
import com.example.utmmart.Model.ModelProduct;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainProfileActivity extends AppCompatActivity {

    private TextView usernameTv, addressTv, statusTv, tabProductTv, tabOrdersTv, filterProductsTv;
    private ImageButton edit_btn, addProduct_btn, filterProductBtn, home_btn;
    private ImageView ProfileIv ;
    private RecyclerView productsRv, ordersRv;
    private RelativeLayout productsR1, ordersR1;
    private EditText searchProductEt;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    private ArrayList<ModelProduct> productList;
    private AdapterProduct adapterProduct;

    private ArrayList<ModelOrder> ordersList;
    private AdapterOrder adapterOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_profile);

        usernameTv = findViewById(R.id.usernameTv);
        edit_btn = findViewById(R.id.edit_btn);
        home_btn = findViewById(R.id.home_btn);
        addressTv = findViewById(R.id.addressTv);
        statusTv = findViewById(R.id.statusTv);
        addProduct_btn = findViewById(R.id.addProduct_btn);
        ProfileIv = findViewById(R.id.ProfileIv);
        tabProductTv = findViewById(R.id.tabProductTv);
        tabOrdersTv = findViewById(R.id.tabOrdersTv);
        productsR1 = findViewById(R.id.productsR1);
        ordersRv = findViewById(R.id.ordersRv);
        ordersR1 = findViewById(R.id.ordersR1);
        filterProductsTv = findViewById(R.id.filterProductsTv);
        filterProductBtn = findViewById(R.id.filterProductBtn);
        productsRv = findViewById(R.id.productsRv);
        searchProductEt = findViewById(R.id.searchProductEt);


        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCanceledOnTouchOutside(false);
        firebaseAuth = firebaseAuth.getInstance();
        checkUser();
        loadAllProducts();

        showProductsUI();

        searchProductEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    adapterProduct.getFilter().filter(s);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(MainProfileActivity.this,EditProfileActivity.class));
            }
        });

        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(MainProfileActivity.this,home.class));
            }
        });

        addProduct_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainProfileActivity.this,addNewProductActivity.class));
            }

        });


        tabProductTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProductsUI();

            }
        });

        tabOrdersTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showOrdersUI();

            }
        });

        filterProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainProfileActivity.this);
                builder.setTitle("Choose Category")
                        .setItems(Constant.productCategories1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String selected = Constant.productCategories1[which];
                                filterProductsTv.setText(selected);
                                if (selected.equals("All")){

                                    loadAllProducts();
                                }
                                else {
                                    loadFilterProduct(selected);
                                }

                            }
                        })
                        .show();
            }
        });
    }

    private void loadFilterProduct(String selected) {
        productList = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Products");
        reference.child(firebaseAuth.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        productList.clear();
                        for (DataSnapshot ds: dataSnapshot.getChildren()){

                            String productCategory = ""+ds.child("productCategory").getValue();

                            if (selected.equals(productCategory)){
                                ModelProduct modelProduct = ds.getValue(ModelProduct.class);
                                productList.add(modelProduct);
                            }


                        }
                        adapterProduct = new AdapterProduct(MainProfileActivity.this, productList);

                        productsRv.setAdapter(adapterProduct);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

    }

    private void loadAllProducts() {
        productList = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Products");
        reference.child(firebaseAuth.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        productList.clear();
                        for (DataSnapshot ds: dataSnapshot.getChildren()){
                            ModelProduct modelProduct = ds.getValue(ModelProduct.class);
                            productList.add(modelProduct);
                        }
                        adapterProduct = new AdapterProduct(MainProfileActivity.this, productList);

                        productsRv.setAdapter(adapterProduct);
                    }
                    // }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

    }

    private void showProductsUI() {

        productsR1.setVisibility(View.VISIBLE);
        ordersR1.setVisibility(View.GONE);

        tabProductTv.setTextColor(getResources().getColor(R.color.black));
        tabProductTv.setBackgroundResource(R.drawable.bg_colour4);

        tabOrdersTv.setTextColor(getResources().getColor(R.color.white));
        tabOrdersTv.setBackgroundResource(R.drawable.bg_colour3);
    }


    private void showOrdersUI() {

        productsR1.setVisibility(View.GONE);
        ordersR1.setVisibility(View.VISIBLE);

        tabProductTv.setTextColor(getResources().getColor(R.color.white));
        tabProductTv.setBackgroundResource(R.drawable.bg_colour3);

        tabOrdersTv.setTextColor(getResources().getColor(R.color.black));
        tabOrdersTv.setBackgroundResource(R.drawable.bg_colour4);


    }


    private void checkUser() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user == null){
            startActivity(new Intent(MainProfileActivity.this, login.class));
            finish();
        }
        else {
            loadMyInfo();
        }
    }

    private void loadMyInfo() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.child(firebaseAuth.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        // for (DataSnapshot ds: dataSnapshot.getChildren()){
                        String username = ""+dataSnapshot.child("username").getValue();
                        String address = ""+dataSnapshot.child("address").getValue();
                        String status = ""+dataSnapshot.child("status").getValue();
                        String profile3 = ""+dataSnapshot.child("profile_image").getValue();

                        usernameTv.setText(username);
                        addressTv.setText(address);
                        statusTv.setText(status);

                        try {
                            Picasso.get().load(profile3).placeholder(R.drawable.icon_profile).into(ProfileIv);
                        }
                        catch (Exception e){
                            ProfileIv.setImageResource(R.drawable.icon_profile);
                        }

                        loadOrders();
                    }
                    // }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });



    }

    private void loadOrders() {

        ordersList = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Orders");
        reference.child(firebaseAuth.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ordersList.clear();

                        for (DataSnapshot ds: dataSnapshot.getChildren()){
                            ModelOrder modelOrder = ds.getValue(ModelOrder.class);
                            ordersList.add(modelOrder);
                        }
                        adapterOrder = new AdapterOrder(MainProfileActivity.this, ordersList);
                        ordersRv.setAdapter(adapterOrder);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }


}