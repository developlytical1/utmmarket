package com.example.utmmart;

public class Register {

    private String uid;
    private String name;
    private String email;
    private String username;
    private String address;
    private String contact;
    private String profile_image;
    private String status;

    public Register() {
    }

    public Register(String uid, String name, String email, String username, String address, String contact, String profile_image, String status) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.username = username;
        this.address = address;
        this.contact = contact;
        this.profile_image = profile_image;
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}