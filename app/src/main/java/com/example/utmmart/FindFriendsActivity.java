package com.example.utmmart;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toolbar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindFriendsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private RecyclerView FindFriendsRecycleList;
    private DatabaseReference UserReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);
        UserReference = FirebaseDatabase.getInstance().getReference().child("Users");
        FindFriendsRecycleList = (RecyclerView) findViewById(R.id.find_friends_recyclelist);
        FindFriendsRecycleList.setLayoutManager(new LinearLayoutManager(this));

        setSupportActionBar(findViewById(R.id.find_friend_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Find Friends");
    }

    protected void onStart() {

        super.onStart();
        FirebaseRecyclerOptions<Register> options =
                new FirebaseRecyclerOptions.Builder<Register>().setQuery(UserReference,Register.class).build();

        FirebaseRecyclerAdapter<Register,FindFriendViewHoler> adapter = new FirebaseRecyclerAdapter<Register, FindFriendViewHoler>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FindFriendViewHoler holder, int position, @NonNull Register model) {
                holder.username.setText(model.getName());
                holder.userStatus.setText(model.getStatus());
                if (model.getProfile_image().isEmpty())
                    Picasso.get().load(R.drawable.profile_message).placeholder(R.drawable.profile_message).into(holder.profileImage);
                //tambah
                else
                    Picasso.get().load(model.getProfile_image()).placeholder(R.drawable.profile_message).into(holder.profileImage);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String visit_user_id  = getRef(position).getKey();

                        //tukar ke page nasrin editprofile
                        Intent profileIntent = new Intent(FindFriendsActivity.this,ProfileActivityMessage.class);
                        profileIntent.putExtra("visit_user_id",visit_user_id);
                        startActivity(profileIntent);

                    }
                });
            }

            @NonNull
            @Override
            //display layout :view user display layout
            public FindFriendViewHoler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.users_display_layout_message,viewGroup,false);
                FindFriendViewHoler viewHoler = new FindFriendViewHoler(view);
                return viewHoler;
            }
        };

        FindFriendsRecycleList.setAdapter(adapter);
        adapter.startListening();
    }


    public static class FindFriendViewHoler extends RecyclerView.ViewHolder{
        TextView username,userStatus;
        CircleImageView profileImage;
        public FindFriendViewHoler(@NonNull View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.user_profile_name);
            userStatus = itemView.findViewById(R.id.user_status);
            profileImage = itemView.findViewById(R.id.users_profile_image_message);


        }
    }
}