package com.example.utmmart;
//message
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

//message
public class ContactFragments extends Fragment {

    private View ContactView;
    private RecyclerView myContactList;

    private DatabaseReference ContactRef ,UserRef;
    private FirebaseAuth mAuth;
    private String currentuserID;

    public ContactFragments() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //inflate the layout for this fragment
        ContactView = inflater.inflate(R.layout.fragment_contact_fragments, container, false);
        myContactList = (RecyclerView) ContactView.findViewById(R.id.contacts_list);
        myContactList.setLayoutManager(new LinearLayoutManager(getContext()));
        mAuth = FirebaseAuth.getInstance();
        currentuserID = mAuth.getCurrentUser().getUid();
        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contacts")
                .child(currentuserID);
        UserRef= FirebaseDatabase.getInstance().getReference().child("Users");

        return  ContactView;
    }

    public void onStart()
    {
        super.onStart();
        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(ContactRef,Contacts.class).build();

        final  FirebaseRecyclerAdapter<Contacts,ContactViewsHolder>adapter = new FirebaseRecyclerAdapter<Contacts, ContactViewsHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull final ContactViewsHolder holder, int position, @NonNull Contacts model) {
                final  String userId = getRef(position).getKey();
                UserRef.child(userId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        if (snapshot.exists())
                        {
                            if(snapshot.child("userState").hasChild("state"))
                            {
                                String state = snapshot.child("userState").child("state").getValue().toString();
                                String date = snapshot.child("userState").child("date").getValue().toString();
                                String time = snapshot.child("userState").child("time").getValue().toString();

                                if (state.equals("online"))
                                {
                                    holder.onlineIcon.setVisibility(View.VISIBLE);
                                }

                                else if (state.equals("offline"))
                                {
                                    holder.onlineIcon.setVisibility(View.INVISIBLE);
                                }
                            }

                            else
                            {
                                holder.onlineIcon.setVisibility(View.INVISIBLE);
                            }

                            if (snapshot.hasChild("profile_image"))
                            {
                                String userImage = snapshot.child("profile_image").getValue().toString();
                                String profileName = snapshot.child("username").getValue().toString();
                                String profileStatus = snapshot.child("status").getValue().toString();

                                holder.userName.setText(profileName);
                                holder.userStatus.setText(profileStatus);
                                if (userImage.isEmpty())
                                    Picasso.get().load(R.drawable.profile_message).placeholder(R.drawable.profile_message).into(holder.profileImage);
                                else
                                    //tambah
                                    Picasso.get().load(userImage).placeholder(R.drawable.profile_message).into(holder.profileImage);
                            }
                            else
                            {
                                String profileName = snapshot.child("username").getValue().toString();
                                String profileStatus = snapshot.child("status").getValue().toString();

                                holder.userName.setText(profileName);
                                holder.userStatus.setText(profileStatus);

                            }
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

            }

            @Override
            public ContactViewsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.users_display_layout_message,viewGroup,false);
                ContactViewsHolder viewsHolder = new ContactViewsHolder(view);
                return viewsHolder;
            }
        };

        myContactList.setAdapter(adapter);
        adapter.startListening();

    }

    public static class ContactViewsHolder extends RecyclerView.ViewHolder
    {
        TextView userName , userStatus;
        CircleImageView profileImage;
        ImageView onlineIcon;
        public ContactViewsHolder(@NonNull View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.user_profile_name);
            userStatus = itemView.findViewById(R.id.user_status);
            profileImage = itemView.findViewById(R.id.users_profile_image_message);
            onlineIcon = (ImageView) itemView.findViewById(R.id.users_online_status);
        }
    }
}