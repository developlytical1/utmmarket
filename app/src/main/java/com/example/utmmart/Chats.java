package com.example.utmmart;
//message
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class Chats extends Fragment {

    private View PrivateChatView;
    private RecyclerView chatsLits;
    private DatabaseReference chatsRef,userRef;
    private FirebaseAuth mAuth;
    private String currentUserID= " ";

    public Chats() {
        // Required empty public constructor
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        PrivateChatView = inflater.inflate(R.layout.fragment_chats,container,false);
        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        chatsRef = FirebaseDatabase.getInstance().getReference().child("Contacts").child(currentUserID);
        userRef = FirebaseDatabase.getInstance().getReference().child("Users");
        chatsLits =(RecyclerView) PrivateChatView.findViewById(R.id.chats_list);
        chatsLits.setLayoutManager(new LinearLayoutManager(getContext()));

        return PrivateChatView;
    }

    public void onStart() {

        super.onStart();

        FirebaseRecyclerOptions<Contacts> options =
                new FirebaseRecyclerOptions.Builder<Contacts>().setQuery(chatsRef,Contacts.class).build();


        FirebaseRecyclerAdapter <Contacts,ChatsViewHolder> adapter = new FirebaseRecyclerAdapter<Contacts, ChatsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ChatsViewHolder chatsViewHolder, int i, @NonNull Contacts contacts) {
                final String usersIDs = getRef(i).getKey();
                final String[] retImage = {"default_image"};
                userRef.child(usersIDs).addValueEventListener(new ValueEventListener() {
                    @Override
                    //because image is option
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.exists()){
                            if (!snapshot.child("profile_image").getValue().toString().isEmpty()){
                                retImage[0] = snapshot.child("profile_image").getValue().toString();
                                //display image:import picasso from built gradle
                                Picasso.get().load(retImage[0]).into(chatsViewHolder.profileImage);
                            }
                            final String retName = snapshot.child("username").getValue().toString();
                            final String retStatus = snapshot.child("status").getValue().toString();

                            chatsViewHolder.userName.setText(retName);

                            //   chatsViewHolder.userStatus.setText("Last Seen : " + "\n" +"Date" + "Time");

                            if(snapshot.child("userState").hasChild("state"))
                            {
                                String state = snapshot.child("userState").child("state").getValue().toString();
                                String date = snapshot.child("userState").child("date").getValue().toString();
                                String time = snapshot.child("userState").child("time").getValue().toString();

                                if (state.equals("online"))
                                {
                                    chatsViewHolder.userStatus.setText("online");
                                }

                                else if (state.equals("offline"))
                                {
                                    chatsViewHolder.userStatus.setText("Last Seen : " + date + "  " + time);
                                }
                            }

                            else
                            {
                                chatsViewHolder.userStatus.setText("offline");
                            }



                            chatsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent chatIntent = new Intent(getContext(),ChatActivity.class);
                                    chatIntent.putExtra("visit_user_id",usersIDs);
                                    chatIntent.putExtra("visit_user_name",retName);
                                    chatIntent.putExtra("visit_image", retImage[0]);
                                    startActivity(chatIntent);
                                }
                            });
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @NonNull
            @Override
            public ChatsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_display_layout_message,parent,false);
                return new ChatsViewHolder(view);
            }
        };

        chatsLits.setAdapter(adapter);
        adapter.startListening();
    }

    public static class ChatsViewHolder extends RecyclerView.ViewHolder{

        CircleImageView profileImage;
        TextView  userStatus,userName;

        public ChatsViewHolder(@NonNull View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.users_profile_image_message);
            userStatus = itemView.findViewById(R.id.user_status);
            userName = itemView.findViewById(R.id.user_profile_name);

        }
    }

}