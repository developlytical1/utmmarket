package com.example.utmmart;

public class Contacts {
    public String username, status, image;

    public Contacts() {
    }

    public Contacts(String name, String status, String image) {
        this.username = name;
        this.status = status;
        this.image = image;
    }

    public String getName() {
        return username;
    }

    public void setName(String name) {
        this.username = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}